﻿using Contacts.Model.Enumerations;
using System.Collections.Generic;

namespace Contacts.Model
{
    public class Contact : BaseResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Status { get; set; }
    }
}
