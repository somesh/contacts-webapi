﻿namespace Contacts.Model.Enumerations
{
    public enum Status
    {
        InActive = 0,
        Active = 1
    }
}
