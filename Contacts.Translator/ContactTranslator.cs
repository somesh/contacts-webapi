﻿using System;
using System.Collections.Generic;

namespace Contacts.Translator
{
    public static class ContactTranslator
    {
        public static Model.Contact ToModel(this Data.Contact contact)
        {
            if (contact == null) return null;

            Enum.TryParse(contact.Status.ToString(), true, out Model.Enumerations.Status status);

            return new Model.Contact()
            {
                Id = contact._id,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                PhoneNumber = contact.PhoneNumber,
                Status = status.ToString()
            };
        }
        public static IEnumerable<Model.Contact> ToModel(this IEnumerable<Data.Contact> contactList)
        {
            if (contactList == null) return null;
            var contactModelList = new List<Model.Contact>();

            foreach (var contact in contactList)
            {
                var contactModel = contact.ToModel();

                if (contactModel == null) continue;

                contactModelList.Add(contactModel);
            }

            return contactModelList;
        }

        public static Data.Contact ToData(this Model.Contact contact)
        {
            if (contact == null) return null;

            Enum.TryParse(contact.Status.ToString(), true, out Data.Enumerations.Status status);

            return new Data.Contact()
            {
                _id = contact.Id,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                PhoneNumber = contact.PhoneNumber,
                Status = status
            };
        }

        public static IEnumerable<Data.Contact> ToData(this IEnumerable<Model.Contact> contactList)
        {
            if (contactList == null) return null;
            var contactModelList = new List<Data.Contact>();

            foreach (var contact in contactList)
            {
                var contactModel = contact.ToData();

                if (contactModel == null) continue;

                contactModelList.Add(contactModel);
            }

            return contactModelList;
        }
    }
}
