﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Contacts.Api.ErrorHandling
{
    public sealed class ErrorInfo
    {
        private ErrorInfo() { }

        public string Code { get; }

        public string Message { get; }

        public HttpStatusCode HttpStatusCode { get; }

        public List<Info> Info { get; private set; }

        public ErrorInfo(string errorCode, string errorMessage, HttpStatusCode httpStatusCode, List<Info> infos = null)
        {
            if (string.IsNullOrWhiteSpace(errorCode))
                throw new ArgumentNullException(nameof(errorCode));

            if (string.IsNullOrWhiteSpace(errorMessage))
                throw new ArgumentNullException(nameof(errorMessage));

            Code = errorCode;
            Message = errorMessage;
            HttpStatusCode = httpStatusCode;
            Info = infos ?? new List<Info>();
        }

        public static ErrorInfo FromBaseApplicationException(BaseApplicationException ex)
        {
            var errorInfo = new ErrorInfo(ex.ErrorCode, ex.ErrorMessage, ex.HttpStatusCode);
            errorInfo.Info.AddRange(ex.Info);
            return errorInfo;
        }

        public static ErrorInfo InternalServerError()
        {
            return new ErrorInfo("99", "Internal server error", HttpStatusCode.InternalServerError);
        }
    }

}
