﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Contacts.Api.ErrorHandling
{
    public abstract class BaseApplicationException : Exception
    {
        #region Properties
        public string ErrorCode { get; }
        public string ErrorMessage { get; }
        public HttpStatusCode HttpStatusCode { get; }

        public List<Info> Info { get; } = new List<Info>();

        #endregion

        #region constructors      


        public BaseApplicationException(string errorCode, string errorMessage, HttpStatusCode httpStatusCode, List<Info> info = null) : base(errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
            this.HttpStatusCode = httpStatusCode;
            if (info != null)
                Info.AddRange(info);
        }

        public BaseApplicationException(string code, string message, HttpStatusCode httpStatusCode, Exception inner) : base(message, inner)
        {
            this.ErrorCode = code;
            this.ErrorMessage = message;
            this.HttpStatusCode = httpStatusCode;
        }

        #endregion

        #region Methods

        public ErrorInfo GetErrorInfo()
        {
            var errorInfo = new ErrorInfo(ErrorCode, ErrorMessage, HttpStatusCode);
            errorInfo.Info.AddRange(Info);
            return errorInfo;
        }

        #endregion
    }
}
