﻿using System;
using System.Collections.Generic;
using Contacts.Model;
using Contacts.Data.Interfaces;
using Contacts.Translator;
using Microsoft.AspNetCore.Mvc;
using Contacts.Data;
using System.Linq;
using System.Threading.Tasks;
using Contacts.Api.ErrorHandling;

namespace Contacts.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public class ContactsController : Controller
    {
        private readonly IContactRepository _contactRepository;

        public ContactsController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var contactList = await _contactRepository.GetAll();

            return Ok(contactList.ToModel());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
                return null;

            var contact = await _contactRepository.Get(id);

            if (contact == null)
                return NotFound(string.Format("Contact not found."));

            return Ok(contact.ToModel());
        }

        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Model.Contact contact)
        {
            var contactData = contact.ToData();
            if (contactData == null)
            {
                var error = new ErrorInfo("101", "Contact is null.", System.Net.HttpStatusCode.BadRequest);
                return BadRequest(error);
            }

            var errors = contactData.IsValid();
            if (errors.Count == 0)
            {
                if (_contactRepository.IsExist(contactData))
                {
                    errors.Add("Contact already exists.");
                }
            }

            if (errors.Any())
            {
                var infoList = new List<Info>();
                foreach (var item in errors)
                {
                    infoList.Add(new Info("101", item));
                }

                var error = new ErrorInfo("101", "Following error occured:", System.Net.HttpStatusCode.BadRequest, infoList);
                return BadRequest(error);
            }

            var result = await _contactRepository.Add(contactData);

            return Ok(result);
        }

        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] Model.Contact contact)
        {
            var contactData = contact.ToData();
            if (contactData == null)
            {
                var error = new ErrorInfo("101", "Contact is null.", System.Net.HttpStatusCode.BadRequest);
                return BadRequest(error);
            }

            var errors = contactData.IsValid();

            if (errors.Any())
            {
                var infoList = new List<Info>();
                foreach (var item in errors)
                {
                    infoList.Add(new Info("101", item));
                }

                var error = new ErrorInfo("101", "Following error occured:", System.Net.HttpStatusCode.BadRequest, infoList);
                return BadRequest(error);
            }
            var result = await _contactRepository.Update(contactData);

            if (result == false)
                return NotFound(string.Format("Contact not found."));

            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 0)
            {
                var error = new ErrorInfo("101", "Invalid Id", System.Net.HttpStatusCode.BadRequest);
                return BadRequest(error);
            }
            else
            {
                var result = await _contactRepository.Remove(id);

                if (result == false)
                    return NotFound(string.Format("Contact not found with Id: {0}", id));

               return Ok(result);
            }
        }

        [HttpDelete("RemoveAll")]
        public async Task<IActionResult> RemoveAll()
        {
            return Ok(await _contactRepository.RemoveAll());
        }
    }
}
