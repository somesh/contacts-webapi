﻿namespace Contacts.Data
{
    public class AppSettings
    {
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
