﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Contacts.Data
{
    public class ContactsContext
    {
        private readonly IMongoDatabase _database = null;

        public ContactsContext(IOptions<AppSettings> appSettings)
        {
            var client = new MongoClient(appSettings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(appSettings.Value.Database);
        }

        public IMongoCollection<Contact> Contacts
        {
            get
            {
                return _database.GetCollection<Contact>("Contacts");
            }
        }
    }
}
