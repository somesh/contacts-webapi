﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

using Contacts.Data.Interfaces;
using MongoDB.Bson;

namespace Contacts.Data
{
    public class ContactRepository : IContactRepository
    {
        private readonly ContactsContext _context = null;

        public ContactRepository(IOptions<AppSettings> settings)
        {
            _context = new ContactsContext(settings);
        }

        public bool IsExist(Contact contact)
        {
            try
            {
                var count = _context.Contacts
                                .Find(o => o.Email == contact.Email.Trim() || o.PhoneNumber == contact.PhoneNumber.Trim())
                                .ToList()
                                .Count;

                return count > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Contact>> GetAll()
        {
            try
            {
                return await _context.Contacts.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Contact> Get(int id)
        {
            try
            {
                return await _context.Contacts
                                .Find(o => o._id == id)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Add(Contact contact)
        {
            try
            {
                var lastContact = _context.Contacts.Find(_ => true).SortByDescending(o => o._id).FirstOrDefault();

                if (lastContact == null)
                    contact._id = 1;
                else
                    contact._id = lastContact._id + 1;

                await _context.Contacts.InsertOneAsync(contact);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(int id)
        {
            try
            {
                DeleteResult actionResult = await _context.Contacts.DeleteOneAsync(
                     Builders<Contact>.Filter.Eq("_id", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateStatus(int id, Enumerations.Status status)
        {
            var filter = Builders<Contact>.Filter.Eq(s => s._id, id);
            var update = Builders<Contact>.Update
                            .Set(s => s.Status, status);
            //.CurrentDate(s => s.UpdatedOn);

            try
            {
                UpdateResult actionResult = await _context.Contacts.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(Contact contact)
        {
            try
            {
                var isValid = contact.IsValid();
                if (isValid.Count == 0)
                {
                    ReplaceOneResult actionResult = await _context.Contacts
                                                //.UpdateOne(n => n.Id.Equals(contact.Id), contact, new UpdateOptions { IsUpsert = true });
                                                .ReplaceOneAsync(n => n._id.Equals(contact._id), contact, new UpdateOptions { IsUpsert = true });

                    return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.Contacts.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
