﻿using Contacts.Data.Enumerations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Contacts.Data
{
    public class Contact
    {
        //[BsonId]
        //public ObjectId InternalId { get; set; }

        public int _id { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Status Status { get; set; }

        //public BsonDateTime UpdatedOn { get; set; }
    }
}
