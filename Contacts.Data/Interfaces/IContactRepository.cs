﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contacts.Data.Interfaces
{
    public interface IContactRepository
    {
        Task<IEnumerable<Contact>> GetAll();

        Task<Contact> Get(int id);

        Task<bool> Add(Contact contact);

        Task<bool> Update(Contact contact);

        Task<bool> UpdateStatus(int id, Enumerations.Status status);

        Task<bool> Remove(int id);

        Task<bool> RemoveAll();

        bool IsExist(Contact contact);
    }
}
