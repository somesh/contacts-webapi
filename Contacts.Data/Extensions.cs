﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Data
{
    public static class Extensions
    {
        public static List<string> IsValid(this Contact contact)
        {
            var errors = new List<string>();
            if (contact == null)
            {
                errors.Add("Contact cannot be null.");
                return errors;
            }
            if (string.IsNullOrEmpty(contact.FirstName))
            {
                errors.Add("Invalid firstname.");
            }
            if (string.IsNullOrEmpty(contact.LastName))
            {
                errors.Add("Invalid lastname.");
            }
            if (string.IsNullOrEmpty(contact.Email))
            {
                errors.Add("Invalid email.");
            }
            if (string.IsNullOrEmpty(contact.PhoneNumber))
            {
                errors.Add("Invalid phone number.");
            }
            return errors;
        }
    }
}
