# Contacts Web API

Contacts Web APIs with .Net Core 2.0 WebAPI and MongoDB as a service.

## Prerequisites

```
* .Net Core 2.0
* Visual Studio 2017 or Visual Studio Code (if want to run or debug code)
```

### Guidelines

This document provides guidelines and examples for Contacts Web APIs.

#### URL Structure

  /api/v1/Contacts/

#### Methods:
```
* GetAll:
 'Method type': GET
 'Usage': Used to get all contacts stored
	** No parameter required
	** Success Response:
	* **Code:** 200
	* **Content:** `[
		{
			"id": 1,
			"firstName": "FirstName",
			"lastName": "LastName",
			"email": "name@gmail.com",
			"phoneNumber": "1234567890",
			"status": "Active"
		},
		{
			"id": 2,
			"firstName": "FirstName",
			"lastName": "LastName",
			"email": "name1@gmail.com",
			"phoneNumber": "1234567890",
			"status": "Active"
		}
	]`

* **Sample Call:**

  ```javascript
	$.ajax({
	    url: "/api/v1/Contacts/GetAll",
	    dataType: "json",
	    type : "GET",
	    success : function(result) {
	      console.log(result);
	    }
	  });

* Get:
 'Method type': GET
 'Usage': Get a contact by id
	** Parameter
		Id
	** Success Response:
	* **Code:** 200
	* **Content:** 
	`{
		"id": 1,
		"firstName": "FirstName",
		"lastName": "LastName",
		"email": "name@gmail.com",
		"phoneNumber": "1234567890",
		"status": "Active"
	}`

* **Sample Call:**

  ```javascript
	$.ajax({
	    url: "/api/v1/Contacts/1",
	    dataType: "json",
	    type : "GET",
	    success : function(result) {
	      console.log(result);
	    }
	  });

* Add:
 'Method type': POST
 'Usage': Add a contact
	** Request body
		`{
			"FirstName": "FirstName",
			"LastName": "LastName",
			"Email": "name@gmail.com",
			"PhoneNumber": "1234567890",
			"Status": "Active"
		}`
	
	** Success Response:
	* **Code:** 200
	* **Content:** 
	`true`

	** Error Response:
	* **Code:** 400 Bad Request
	* **Content:**
	'{
		"code": "101",
		"message": "Following error occured:",
		"httpStatusCode": 400,
		"info": [
			{
				"code": "101",
				"message": "Contact already exists."
			}
		]
	}'

* **Sample Call:**

  ```javascript
	$.ajax({
    url: "/api/v1/Contacts/Add",
	data: "{
			"FirstName": "FirstName",
			"LastName": "LastName",
			"Email": "name@gmail.com",
			"PhoneNumber": "1234567890",
			"Status": "Active"
		}",
    dataType: "json",
    type : "POST",
    success : function(result) {
      console.log(result);
    }
  });

* Update:
 'Method type': POST
 'Usage': Update a contact
	** Request body
		`{
			"Id": 1
			"FirstName": "FirstName",
			"LastName": "LastName",
			"Email": "name@gmail.com",
			"PhoneNumber": "1234567890",
			"Status": "Active"
		}`
	
	** Success Response:
	* **Code:** 200
	* **Content:** 
	`true`

	** Error Response:
	* **Code:** 404 Bad Request
	* **Content:**
	'{
		"code": "101",
		"message": "Following error occured:",
		"httpStatusCode": 404,
		"info": [
			{
				"code": "101",
				"message": "Contact not found."
			}
		]
	}'

* **Sample Call:**

  ```javascript
	$.ajax({
    url: "/api/v1/Contacts/Update",
	data: "{
			"Id": 1
			"FirstName": "FirstName",
			"LastName": "LastName",
			"Email": "name@gmail.com",
			"PhoneNumber": "1234567890",
			"Status": "Active"
		}",
    dataType: "json",
    type : "POST",
    success : function(result) {
      console.log(result);
    }
  });
  
* Remove:
 'Method type': Delete
 'Usage': Deletes a contact by id
	** Parameter
		Id	
	** Success Response:
	* **Code:** 200
	* **Content:** 
	`true`

	** Error Response:	
	* **Code:** 404 Not found
	* **Content:**
	'Contact not found with Id: {ContactId}'

* **Sample Call:**

  ```javascript
	$.ajax({
	    url: "/api/v1/Contacts/2",
	    dataType: "json",
	    type : "DELETE",
	    success : function(result) {
	      console.log(result);
	    }
	  });

* RemoveAll:
 'Method type': Delete
 'Usage': Deletes all the contacts
	** No parameter required
	** Success Response:
	* **Code:** 200
	* **Content:** `true`

* **Sample Call:**

  ```javascript
	$.ajax({
	    url: "/api/v1/Contacts/RemoveAll",
	    dataType: "json",
	    type : "Delete",
	    success : function(result) {
	      console.log(result);
	    }
	  });